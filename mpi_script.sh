#!/bin/bash

# Copy/paste this job script into a text file and submit with the command:
#    sbatch thefilename
# job standard output will go to the file slurm-%j.out (where %j is the job ID)

#SBATCH --time=2:00:00   # walltime limit (HH:MM:SS)
#SBATCH --nodes=8   # number of nodes
#SBATCH --ntasks=8
#SBATCH --cpus-per-task=36
#SBATCH --ntasks-per-node=1   # 36 processor core(s) per node 
#SBATCH --job-name="mce-mpi"
#SBATCH --mail-user=adas@iastate.edu   # email address
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END

# LOAD MODULES, INSERT CODE, AND RUN YOUR PROGRAMS HERE

#module load gcc/7.3.0-xegsmw4
#module load openmpi/3.1.3-flhsk2d
#module load libpthread-stubs/0.4-ysy56bb
#module load mpich/3.2.1-niuhmad
#module load intel/18.3
#module load numactl/2.0.11-2kixr7l

#module load openmpi
#module load gcc
module load intel/18.3



echo "Running mce in a distributed system with 2 nodes"

now=$(date +"%T")
echo "Current time : $now"

#mpirun -np 72 ./mpi_test /work/baskarg/adas/undirected/as-skitter-edges 36 output
srun ./mpi_test /work/baskarg/adas/undirected/orkut-edges 36 output

now=$(date +"%T")
echo "Current time : $now"

/*
 * main.cpp
 *
 *  Created on: Sep 24, 2018
 *      Author: apurba
 */

#include <iostream>
#include<unordered_set>
#include<vector>
#include <chrono>
using namespace std;

#include "source/tomita.h"
#include "source/undirectedgraph.h"
#include "source/sorttest.h"
#include "source/ParTomita.h"
#include "source/Imce.h"
#include "source/ParImce.h"

int main(int argc, char **argv){

	undirectedgraph g;

	cout << argv[1] << "\n";

	//g.readInAdjList(argv[1]);

	auto t_start = std::chrono::high_resolution_clock::now();

	//g.readInEdgeList(argv[1]);

	g.readInAdjList(argv[1]);

	auto t_end = std::chrono::high_resolution_clock::now();

	auto graph_reading_time = std::chrono::duration<double, std::milli>(t_end - t_start).count();


	//g.print();

	cout << "number of vertices: " << g.numV() << "\n";
	cout << "number of edges: " << g.numE() << "\n";

	cout << "calling IMCE\n";

	Imce imce(g);

	cout << "graph initialization complete\n";

	imce.run(argv[2], argv[3], stoi(argv[4]), argv[5]);	//clique_file, edge_file, batch_size, out_file

	cout << "calling ParImce\n";

	ParImce parimce(g);

	cout << "graph initialization complete\n";

	parimce.run(argv[2], argv[3], stoi(argv[4]), argv[5]);


}

/*
 * EmbarrasingParallel.cpp
 *
 *  Created on: Sep 26, 2018
 *      Author: apurba
 */

#include <iostream>
#include <cmath>
#include "EmbarrasingParallel.h"

struct PowerFunctor{
	void operator()(const blocked_range<size_t>& range) const {

		for(size_t j = range.begin(); j < range.end(); j++){
			long result = std::pow(2.0, j);
			std::cout << "2 power " << j << " is " << result << "\n";
		}
	}
};

struct PFunctor {
	void operator()(const blocked_range<size_t>& range) const {

		for (size_t j = range.begin(); j < range.end(); j++) {

			if(j%1000000 == 0)
				std::cout << "Parallel iteration " << j << "\n"; 

			double sum = 0.0;
			
			//sleep(1);

			for (long i = 0; i < 1000; i++) {
				
				long result = std::pow(2.0, i);

				//if (i % 2 == 0) // if the remainder of `i/2` is 0
				//	sum += -1 / (2 * i - 1);
				//else
				//	sum += 1 / (2 * i - 1);
			}
		}

	}
};

struct PFunctor_1 {
	void operator()(const blocked_range<size_t>& range) const {

		for (size_t j = range.begin(); j < range.end(); j++) {

			tbb::blocked_range<size_t> range(0, 100);

			auto func = PowerFunctor();

			parallel_for(range, func);
		}

	}
};

EmbarrasingParallel::EmbarrasingParallel() {
	// TODO Auto-generated constructor stub

}

void EmbarrasingParallel::ParallelExp(size_t n) {

	tbb::blocked_range<size_t> range(0, n);

	auto func = PFunctor();
	
	parallel_for(range, func);
}

//The following function is to demonstrate nested parallelism in TBB

void EmbarrasingParallel::ParallelExp_1(size_t n) {

	tbb::blocked_range<size_t> range(0, n);

	auto func = PFunctor_1();

	parallel_for(range, func);
}

void EmbarrasingParallel::Exp(size_t n) {

	for (size_t j = 0; j < n; j++) {

		if(j%1000000 == 0)
			std::cout << "Sequential iteration " << j << "\n"; 
		double sum = 0.0;

		for (long i = 0; i < 1000; i++) {

			long result = std::pow(2.0, i);
			//sleep(1);
			//if (i % 2 == 0) // if the remainder of `i/2` is 0
			//	sum += -1 / (2 * i - 1);
			//else
			//	sum += 1 / (2 * i - 1);
		}
	}
}

EmbarrasingParallel::~EmbarrasingParallel() {
	// TODO Auto-generated destructor stub
}


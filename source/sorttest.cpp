/*
 * sorttest.cpp
 *
 *  Created on: Sep 26, 2018
 *      Author: apurba
 */

#include "sorttest.h"

sorttest::sorttest() {
	// TODO Auto-generated constructor stub

}

sorttest::sorttest(size_t t) {

	ARRAY_SIZE = t;
	myInts.resize(ARRAY_SIZE);
	myParInts.resize(ARRAY_SIZE);

	par_time = 0.0;
	seq_time = 0.0;
}

void sorttest::_populate() {
	static const int LARGE_PRIME = 32364931;
	static const int SMALL_PRIME = 1040803;

	int currentValue = rand();

	for (size_t i = 0; i < ARRAY_SIZE; ++i) {

		currentValue = (currentValue + LARGE_PRIME) % SMALL_PRIME;
		myInts[i] = currentValue;
		myParInts[i] = currentValue;

	}

	sortedInts = myInts;
	sortedParInts = myParInts;
}

bool sorttest::sequentialSort() {

	clock_t begin = clock();

	sort(sortedInts.begin(), sortedInts.end());

	clock_t end = clock();

	seq_time = double(end - begin) / CLOCKS_PER_SEC;

	//Order check
	bool isOrdered = true;
	/*for (size_t i = 0; i < ARRAY_SIZE - 1; ++i) {
		if (sortedInts[i] >= sortedInts[i + 1])
			isOrdered = false;
	}*/

	return isOrdered;
}

double sorttest::seqtime() {
	return seq_time;
}

double sorttest::partime() {
	return par_time;
}

bool sorttest::tbbSort() {

	clock_t begin = clock();

	tbb::parallel_sort(sortedParInts.begin(), sortedParInts.end());

	clock_t end = clock();

	par_time = double(end - begin) / CLOCKS_PER_SEC;

	//Order check
	bool isOrdered = true;
	/*for (size_t i = 0; i < ARRAY_SIZE - 1; ++i) {
		if (sortedParInts[i] >= sortedParInts[i + 1])
			isOrdered = false;
	}*/

	return isOrdered;
}

sorttest::~sorttest() {
	// TODO Auto-generated destructor stub
}


/*
 * ParTomita.h
 *
 *  Created on: Sep 28, 2018
 *      Author: apurba
 */

#ifndef SOURCE_PARTOMITA_H_
#define SOURCE_PARTOMITA_H_

#include "global.h"
#include "utils.h"
#include "undirectedgraph.h"



class ParTomita {
	undirectedgraph _g_;
	tbb::atomic<long> cliquecount;
	void expand(std::vector<int>, std::unordered_set<int> &, std::unordered_set<int> &);
	void seq_expand(std::vector<int>, std::unordered_set<int> &, std::unordered_set<int> &);
	int ParFindPivot(std::unordered_set<int> &, std::unordered_set<int> &, undirectedgraph &);
	int findPivot(const unordered_set<int> &, const unordered_set<int> &);
	void computeNewCand(const unordered_set<int> &, const unordered_set<int> &, const unordered_map<int, int> &, int, unordered_set<int> *);
	void computeNewFini(const unordered_set<int> &, const unordered_set<int> &, const unordered_map<int, int> &, const vector<int> &, int, unordered_set<int> *);
public:
	ParTomita();
	ParTomita(const undirectedgraph &g);
	long numberOfCliques();
	void run();
	virtual ~ParTomita();
};



#endif /* SOURCE_PARTOMITA_H_ */

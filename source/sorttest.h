/*
 * sorttest.h
 *
 *  Created on: Sep 26, 2018
 *      Author: apurba
 */

#ifndef SOURCE_SORTTEST_H_
#define SOURCE_SORTTEST_H_

#include <vector>
#include <ctime>
#include <string>
#include <algorithm>
#include <tbb/concurrent_vector.h>
#include <tbb/parallel_sort.h>

class sorttest {
	long ARRAY_SIZE;
	double seq_time;
	double par_time;
	typedef std::vector<long> Integers;
	typedef tbb::concurrent_vector<long> ParIntegers;
	Integers myInts;
	Integers sortedInts;

	Integers myParInts;
	Integers sortedParInts;
public:
	sorttest();
	sorttest(size_t t);
	void _populate();
	bool sequentialSort();
	bool tbbSort();
	double seqtime();
	double partime();
	virtual ~sorttest();
};

#endif /* SOURCE_SORTTEST_H_ */

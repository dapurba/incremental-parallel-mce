/*
 * PECO.cpp
 *
 *  Created on: Nov 1, 2018
 *      Author: apurba
 */

#include "PECO.h"

PECO::PECO() {
	// TODO Auto-generated constructor stub

}

PECO::PECO(const undirectedgraph &g) {
	_g_ = g;
	cliquecount = 0;
}

void PECO::expand(undirectedgraph &_g_, std::vector<int> k, std::unordered_set<int> &cand,
		std::unordered_set<int> &fini) {
	if (cand.empty() && fini.empty()) {

		cliquecount.fetch_and_increment();

		if (cliquecount % 100000000 == 0)
			cout << cliquecount << " maximal cliques generated\n";

		return;
	}
	if (cand.empty() && !fini.empty())
		return;

	//cout << __LINE__ << " Inside expand, before calling findPivot\n";

	int pivot = findPivot(_g_, cand, fini);

	//cout << __LINE__ << " Inside expand, after calling findPivot\n";

	auto cand_iterator = cand.begin();
	const auto cand_end_iterator = cand.end();
	const auto neighbor_of_pivot_end_iterator = _g_.neighbor(pivot).end();
	while (cand_iterator != cand_end_iterator) {

		if (_g_.neighbor(pivot).find(*cand_iterator)
				== neighbor_of_pivot_end_iterator) {

			k.push_back(*cand_iterator);

			unordered_set<int> cand_q;
			utils::unordered_intersect(cand, _g_.neighbor(*cand_iterator),
					&cand_q);
			unordered_set<int> fini_q;
			utils::unordered_intersect(fini, _g_.neighbor(*cand_iterator),
					&fini_q);

			expand(_g_, k, cand_q, fini_q);

			fini.insert(*cand_iterator);
			cand_iterator = cand.erase(cand_iterator);
			k.pop_back();
		} else {
			cand_iterator++;
		}

	}
}

int PECO::findPivot(undirectedgraph &_g_, const unordered_set<int> &cand,
		const unordered_set<int> &fini) {

	int size = -1;
	int p = -1;

	for (int u : cand) {
		int size_of_q = utils::unordered_intersect_size(_g_.neighbor(u), cand);
		if (size < size_of_q) {
			size = size_of_q;
			p = u;
		}
	}

	for (int u : fini) {
		int size_of_q = utils::unordered_intersect_size(_g_.neighbor(u), cand);
		if (size < size_of_q) {
			size = size_of_q;
			p = u;
		}
	}

	return p;
}

void PECO::runOnDegreeOrdering() {

	tbb::parallel_for_each(_g_.V().begin(), _g_.V().end(), [&](int v) {

		std::vector<int> k;
		std::unordered_set<int> cand;
		std::unordered_set<int> fini;

		undirectedgraph _lg_;

		for(int x : _g_.neighbor(v)){
			for(int y : _g_.neighbor(v)){
				if(_g_.ContainsEdge(x,y)){
					_lg_.addEdge(x,y);
				}
			}
		}


		for(int w : _g_.neighbor(v)) {

			if (_g_.degreeOf(w) > _g_.degreeOf(v))
			cand.insert(w);
			else if (_g_.degreeOf(w) < _g_.degreeOf(v))
			fini.insert(w);
			else {
				if (w > v)
				cand.insert(w);
				else
				fini.insert(w);
			}
		}
		expand(_lg_, k, cand, fini);

	});

}

void PECO::runOnDegeneracyOrdering() {

	_g_.degeneracy();

	tbb::parallel_for_each(_g_.V().begin(), _g_.V().end(), [&](int v) {

		std::vector<int> k;
		std::unordered_set<int> cand;
		std::unordered_set<int> fini;

		undirectedgraph _lg_;

		for(int x : _g_.neighbor(v)){
			for(int y : _g_.neighbor(v)){
				if(_g_.ContainsEdge(x,y)){
					_lg_.addEdge(x,y);
				}
			}
		}

		for(int w : _g_.neighbor(v)) {

			if (_g_.degeneracyOf(w) > _g_.degeneracyOf(v))
			cand.insert(w);
			else if (_g_.degeneracyOf(w) < _g_.degeneracyOf(v))
			fini.insert(w);
			else {
				if (w > v)
				cand.insert(w);
				else
				fini.insert(w);
			}
		}
		expand(_lg_, k, cand, fini);

	});

}

void PECO::runOnTriangleOrdering() {

	_g_.triangleCount();

	tbb::parallel_for_each(_g_.V().begin(), _g_.V().end(), [&](int v) {

		std::vector<int> k;
		std::unordered_set<int> cand;
		std::unordered_set<int> fini;
		
		undirectedgraph _lg_;

		for(int x : _g_.neighbor(v)){
			for(int y : _g_.neighbor(v)){
				if(_g_.ContainsEdge(x,y)){
					_lg_.addEdge(x,y);
				}
			}
		}


		for(int w : _g_.neighbor(v)) {

			if (_g_.triangleCountAt(w) > _g_.triangleCountAt(v))
			cand.insert(w);
			else if (_g_.triangleCountAt(w) < _g_.triangleCountAt(v))
			fini.insert(w);
			else {
				if (w > v)
				cand.insert(w);
				else
				fini.insert(w);
			}
		}
		expand(_lg_, k, cand, fini);

	});

}

long PECO::numberOfCliques() {
	return cliquecount;
}

PECO::~PECO() {
	// TODO Auto-generated destructor stub
}


/*
 * Imce.h
 *
 *  Created on: Oct 10, 2018
 *      Author: apurba
 */

#ifndef SOURCE_IMCE_H_
#define SOURCE_IMCE_H_

#include "global.h"
#include "undirectedgraph.h"
#include "utils.h"

class Imce {
	undirectedgraph _g_;	//the graph that goes through the changes
	undirectedgraph _h_;	//for storing the set of new edges
	long new_clique_count;	//number of new maximal cliques
	long subsumed_clique_count;	//number of subsumed cliques
	unordered_set<string> maxcliques;	//The set of all maximal cliques
	double computation_time_new;
	double computation_time_subsumed;
	void ImceNew(vector<pair<int, int>> &);
	void CreateInducedSubgraph(unordered_set<int>&, undirectedgraph *);
	void EnumNew(undirectedgraph &, int, int, UMap<int, unordered_set<int>> &);
	void expand(undirectedgraph &, set<int> &, unordered_set<int> &, unordered_set<int> &, UMap<int, unordered_set<int>> &);
	int findPivot(undirectedgraph &, const unordered_set<int> &, const unordered_set<int> &);
	void ComputeSubsumedCliques(set<int> &);
public:
	Imce();
	Imce(undirectedgraph &);
	void run(const string &, const string &, int, const string &);
	int getNewCliqueCount();
	int getSubCliqueCount();
	virtual ~Imce();
};

#endif /* SOURCE_IMCE_H_ */

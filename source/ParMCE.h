/*
 * ParMCEDegree.h
 *
 *  Created on: Oct 2, 2018
 *      Author: apurba
 */

#ifndef SOURCE_PARMCE_H_
#define SOURCE_PARMCE_H_

#include <unordered_set>
#include <vector>
#include <unordered_map>
#include <chrono>
#include <tbb/parallel_for_each.h>
#include <tbb/atomic.h>
#include <tbb/concurrent_hash_map.h>
#include <tbb/blocked_range.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_do.h>
#include <tbb/task_scheduler_init.h>
#include "utils.h"
#include "undirectedgraph.h"

using namespace std;

typedef tbb::concurrent_hash_map<int, int> ConcurrentTable;

class ParMCE {
	undirectedgraph _g_;
	tbb::atomic<long> cliquecount;
	string of;
	void expand(std::vector<int>, std::unordered_set<int> &, std::unordered_set<int> &);
	int ParFindPivot(std::unordered_set<int> &, std::unordered_set<int> &, undirectedgraph &);
	void computeNewCand(const unordered_set<int> &, const unordered_set<int> &, const unordered_map<int, int> &, int, unordered_set<int> *);
	void computeNewFini(const unordered_set<int> &, const unordered_set<int> &, const unordered_map<int, int> &, const vector<int> &, int, unordered_set<int> *);
	void seq_expand(std::vector<int>, std::unordered_set<int> &, std::unordered_set<int> &);
	int findPivot(const unordered_set<int> &, const unordered_set<int> &);
public:
	ParMCE();
	ParMCE(const undirectedgraph &);
	void runOnDegreeOrdering(int, string);
	void runOnDegeneracyOrdering(int);
	void runOnTriangleOrdering(int);
	long numberOfCliques();
	void runSequential();
	void runELS();
	virtual ~ParMCE();
};

#endif /* SOURCE_PARMCE_H_ */

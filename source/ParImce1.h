/*
 * ParImce1.h
 *
 *  Created on: Oct 10, 2018
 *      Author: apurba
 */

#ifndef SOURCE_PARIMCE1_H_
#define SOURCE_PARIMCE1_H_


#include "global.h"
#include "utils.h"
#include "undirectedgraph.h"


class ParImce1 {
	undirectedgraph _g_;	//the graph that goes through the changes
	undirectedgraph _h_;	//for storing the set of new edges
	tbb::atomic<long> new_clique_count;	//number of new maximal cliques
	tbb::concurrent_unordered_set<string> subsumed_cliques;	//the set for storing the set of subsumed cliques
	double computation_time_new;
	tbb::atomic<long> computation_time_subsumed;
	void ParImceNew(vector<pair<int, int>> &);
	void CreateInducedSubgraph(unordered_set<int>&, undirectedgraph *);
	void ParEnumNew(undirectedgraph &, int, int, int, UMap<string, int> &);
	void ParExpand(undirectedgraph &, set<int> &, unordered_set<int> &, unordered_set<int> &, int, UMap<string, int> &);
	void ParExpandForSubsumed(undirectedgraph &, set<int> &, unordered_set<int> &, unordered_set<int> &);
	int ParFindPivot(const unordered_set<int> &, const unordered_set<int> &, undirectedgraph &);
	void computeNewCand(const unordered_set<int> &, const unordered_set<int> &, const unordered_map<int, int> &, int, unordered_set<int> *);
	void computeNewFini(const unordered_set<int> &, const unordered_set<int> &, const unordered_map<int, int> &, const vector<int> &, int, unordered_set<int> *);
public:
	ParImce1();
	ParImce1(undirectedgraph &);
	void run(const string &, const string &, int, const string &);

	int getNewCliqueCount();
	int getSubCliqueCount();
	virtual ~ParImce1();
};

#endif /* SOURCE_PARIMCE_H_ */

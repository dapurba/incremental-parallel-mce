/*
 * HybMCE.h
 *
 *  Created on: Feb 11, 2019
 *      Author: apurba
 */

#ifndef SOURCE_HYBMCE_H_
#define SOURCE_HYBMCE_H_

#include "global.h"
#include "utils.h"
#include "undirectedgraph.h"

class HybMCE {
	undirectedgraph _g_;
	tbb::atomic<long> cliquecount;
	int number_of_nodes;
	void expand(std::vector<int>, std::unordered_set<int> &, std::unordered_set<int> &);
	int ParFindPivot(std::unordered_set<int> &, std::unordered_set<int> &, undirectedgraph &);
	void computeNewCand(const unordered_set<int> &, const unordered_set<int> &, const unordered_map<int, int> &, int, unordered_set<int> *);
	void computeNewFini(const unordered_set<int> &, const unordered_set<int> &, const unordered_map<int, int> &, const vector<int> &, int, unordered_set<int> *);
	void seq_expand(std::vector<int>, std::unordered_set<int> &, std::unordered_set<int> &);
	int findPivot(const unordered_set<int> &, const unordered_set<int> &);
public:
	HybMCE();
	//HybMCE(const undirectedgraph &);
	HybMCE(const undirectedgraph &, int);
	void runOnDegreeOrdering(int, int);
	long numberOfCliques();
	void CallParMCE(int, undirectedgraph &);
	virtual ~HybMCE();
};

#endif /* SOURCE_HYBMCE_H_ */

/*
 * EmbarrasingParallel.h
 *
 *  Created on: Sep 26, 2018
 *      Author: apurba
 */

#ifndef SOURCE_EMBARRASINGPARALLEL_H_
#define SOURCE_EMBARRASINGPARALLEL_H_

#include "tbb/parallel_for.h"

using namespace tbb;

class EmbarrasingParallel {
public:
	EmbarrasingParallel();
	void ParallelExp(size_t n);
	void ParallelExp_1(size_t n);
	void Exp(size_t n);
	virtual ~EmbarrasingParallel();
};

#endif /* SOURCE_EMBARRASINGPARALLEL_H_ */

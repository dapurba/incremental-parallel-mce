/*
 * Imce.cpp
 *
 *  Created on: Oct 10, 2018
 *      Author: apurba
 */

#include "Imce.h"


Imce::Imce() {
	// TODO Auto-generated constructor stub

	new_clique_count = 0;
	subsumed_clique_count = 0;
	computation_time_new = 0;
	computation_time_subsumed = 0;

}

Imce::Imce(undirectedgraph &g){
	_g_ = g;
	new_clique_count = 0;
	subsumed_clique_count = 0;
	computation_time_new = 0;
	computation_time_subsumed = 0;

}

void Imce::ImceNew(vector<pair<int, int>>& batch){

	//undirectedgraph _h_;


	//cout << "Inside function " << __func__ << "\n";

	//cout << "batch size " << batch.size() << "\n";

	new_clique_count = 0;
	subsumed_clique_count = 0;

	computation_time_new = 0;
	computation_time_subsumed = 0;

	undirectedgraph h;

	_h_ = h;

	for(auto e : batch){
		int u = e.first;
		int v = e.second;

		_g_.addEdge(u,v);

		//cout << e[0] << " " << e[1] << "\n";

		_h_.addEdge(u,v);
	}

	//cout << "number of vertices of the updated graph: " << _g_.numV() << "\n";
	//cout << "number of edges of the updated graph: " << _g_.numE() << "\n";

	UMap<int, unordered_set<int>> E;

	for(auto e : batch){
		int u = e.first;
		int v = e.second;

		unordered_set<int> common_neighborhood;
		utils::unordered_intersect(_g_.neighbor(u), _g_.neighbor(v), &common_neighborhood);

		common_neighborhood.insert(u);
		common_neighborhood.insert(v);

		undirectedgraph g_e;

		//cout << "common neighborhood size: " << common_neighborhood.size() << "\n";

		CreateInducedSubgraph(common_neighborhood, &g_e);

		//cout << "number of vertices of the subgraph: " << g_e.numV() << "\n";

		//cout << "number of edges of the subgraph: " << g_e.numE() << "\n";

		EnumNew(g_e, u, v, E);

		E[u].insert(v);
		E[v].insert(u);



	}
}

void Imce::EnumNew(undirectedgraph &g, int u, int v, UMap<int, unordered_set<int>>& table){
	set<int> k;
	unordered_set<int> fini;
	unordered_set<int> cand = g.V();

	k.insert(u);
	k.insert(v);

	//cout << "Inside function " << __func__ << "\n";

	expand(g, k, cand, fini, table);
}

void Imce::expand(undirectedgraph &g, set<int> &k, unordered_set<int> &cand, unordered_set<int> &fini, UMap<int, unordered_set<int>> &table){

	//cout << "Inside function " << __func__ << "\n";

	if (cand.empty() && fini.empty()) {

			new_clique_count++;

			//cout << new_clique_count << " new maximal cliques found\n";

			if (new_clique_count % 1000000 == 0)
				cout << new_clique_count << " maximal cliques generated\n";

			string result;

			for(int u : k){
				result += std::to_string(u) + " ";
			}

			maxcliques.insert(result);

			auto start = std::chrono::high_resolution_clock::now();

			ComputeSubsumedCliques(k);

			auto end = std::chrono::high_resolution_clock::now();

			auto elapsed = std::chrono::duration<double, std::milli>(end - start).count();

			computation_time_subsumed += elapsed;

			return;
		}
		if (cand.empty() && !fini.empty())
			return;

		//cout << __LINE__ << " Inside expand, before calling findPivot\n";

		int pivot = findPivot(g, cand, fini);

		//cout << __LINE__ << " Inside expand, after calling findPivot\n";



		auto cand_iterator = cand.begin();
		const auto cand_end_iterator = cand.end();
		const auto neighbor_of_pivot_end_iterator = g.neighbor(pivot).end();
		while (cand_iterator != cand_end_iterator) {


			if (g.neighbor(pivot).find(*cand_iterator) == neighbor_of_pivot_end_iterator) {

				//k.push_back(*cand_iterator);

				k.insert(*cand_iterator);

				bool flag = false;

				if(table.size() > 0){
					for(int v : k){
						if(table[*cand_iterator].find(v) != table[*cand_iterator].end()){
							flag = true;
							break;
						}
					}
				}

				if(flag){
					fini.insert(*cand_iterator);
					k.erase(*cand_iterator);
					cand_iterator = cand.erase(cand_iterator);
					continue;
				}

				unordered_set<int> cand_q;
				utils::unordered_intersect(cand, g.neighbor(*cand_iterator), &cand_q);
				unordered_set<int> fini_q;
				utils::unordered_intersect(fini, g.neighbor(*cand_iterator), &fini_q);

				expand(g, k, cand_q, fini_q, table);


				fini.insert(*cand_iterator);
				k.erase(*cand_iterator);
				cand_iterator = cand.erase(cand_iterator);
				//k.pop_back();


			} else {
				cand_iterator++;
			}

		}

}

void Imce::ComputeSubsumedCliques(set<int>& K){


	//cout << "Inside function " << __func__ << "\n";

	std::vector<set<int>> S[2];

	S[0].push_back(K);

	int idx = 0;

	for(int u : K){
		for(int v : K){
			if((u < v) && _h_.ContainsEdge(u,v)){
				idx = 1-idx;
				for(vector<set<int>>::iterator it = S[1-idx].begin(); it != S[1-idx].end(); it++){
					set<int> t = *it;
					if((t.find(u) != t.end()) && (t.find(v) != t.end())){
						set<int> x(t);
						set<int> y(t);

						x.erase(u);
						y.erase(v);

						S[idx].push_back(x);
						S[idx].push_back(y);
					} else {
						S[idx].push_back(t);
					}
				}
				S[1-idx].clear();
			}
		}
	}

	for(set<int> c : S[idx]){

		string result;

		for(int u : c){
			result += std::to_string(u) + " ";
		}

		if(maxcliques.find(result) != maxcliques.end()){
			subsumed_clique_count++;
			maxcliques.erase(result);
		}

	}


}

int Imce::findPivot(undirectedgraph &g, const unordered_set<int> &cand,
		const unordered_set<int> &fini) {

	int size = -1;
	int p = -1;

	for (int u : cand) {
		int size_of_q = utils::unordered_intersect_size(g.neighbor(u), cand);
		if (size < size_of_q) {
			size = size_of_q;
			p = u;
		}
	}

	for (int u : fini) {
		int size_of_q = utils::unordered_intersect_size(g.neighbor(u), cand);
		if (size < size_of_q) {
			size = size_of_q;
			p = u;
		}
	}

	return p;
}

void Imce::CreateInducedSubgraph(unordered_set<int>& vertices, undirectedgraph *g){

	for (int u : vertices) {
		for (int v : vertices) {
			if (_g_.neighbor(u).find(v) != _g_.neighbor(u).end()) {
				(*g).addEdge(u, v);
			}
		}
	}
}


void Imce::run(const string &clique_file, const string &edge_file, int batch_size, const string &out_fname){

	std::ifstream edgestream(edge_file.c_str());

	std::ifstream cliquestream(clique_file.c_str());



	int index = 0;
	int count = 0;

	string line;

	while(!cliquestream.eof()){

		count++;

		std::getline(cliquestream, line);

		stringstream strm(line);

		int u;

		set<int> clique;

		while(strm >> u){
			clique.insert(u);
		}

		string result;

		for(int u : clique){
			result += std::to_string(u) + " ";
		}

		//	cout << count << " inserting a maximal clique: " <<  result << "\n";

		if(!result.empty())
			maxcliques.insert(result);
	}

	cout << "Reading initial maximal cliques done\n";

	cout << "number of maximal clique of the initial graph: " << maxcliques.size() <<  "\t" << count << "\n";

	ofstream out_file;

	out_file.open(out_fname.c_str(), ios::app);

	out_file << "Results of IMCE on batch size: " << batch_size << "\n";

	out_file << "iteration\tnumber_new\tnumber_subsumed\ttime_new(ms)\ttiem_subsumed(ms)\n";

	out_file.close();

	count = 0;

	while(true){

		index = 0;
		count++;

		vector< pair<int, int>> batch_of_edges;

		while (index < batch_size){
			if(edgestream.good() && !edgestream.eof()){

				std::getline(edgestream, line);
				stringstream strm(line);
				if(!line.empty() && strm.good() && !strm.eof()){
					int u;
					int v;
					strm >> u;
					strm >> v;

					//cout << u << " " << v << "\n";

					//int e[2] = {u,v};

					batch_of_edges.push_back(make_pair(u, v));
				}
				index++;
			} else {
				break;
			}
		}

	    auto start = std::chrono::high_resolution_clock::now();

		ImceNew(batch_of_edges);

		auto end =  std::chrono::high_resolution_clock::now();

		auto elapsed = std::chrono::duration<double, std::milli>(end - start).count();

		computation_time_new = elapsed-computation_time_subsumed;

		out_file.open(out_fname.c_str(), ios::app);

		out_file << count << "\t" << new_clique_count << "\t" << subsumed_clique_count << "\t" << computation_time_new << "\t" << computation_time_subsumed <<"\n";

		cout << count << "\t" << new_clique_count << "\t" << subsumed_clique_count << "\t" << maxcliques.size() << "\n";

		out_file.close();

		if(count == 10)
			break;
	}
}

int Imce::getNewCliqueCount(){
	return new_clique_count;
}

int Imce::getSubCliqueCount(){
	return subsumed_clique_count;
}

Imce::~Imce() {
	// TODO Auto-generated destructor stub
}


/*
 * global.h
 *
 *  Created on: Oct 12, 2018
 *      Author: apurba
 */

#ifndef SOURCE_GLOBAL_H_
#define SOURCE_GLOBAL_H_

#include <string>
#include <iostream>
#include <vector>
#include <chrono>
#include <unordered_set>
#include <unordered_map>
#include <tbb/atomic.h>
#include <tbb/concurrent_hash_map.h>
#include <tbb/concurrent_unordered_set.h>
#include <tbb/concurrent_vector.h>
#include <tbb/blocked_range.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_do.h>
#include <tbb/parallel_for_each.h>
#include <tbb/tick_count.h>
#include <tbb/task_scheduler_init.h>

using namespace std;
using namespace tbb;


template<typename K, typename V>
using ConcurrentMap = tbb::concurrent_hash_map<K,V>;

template<typename K, typename V>
using UMap = unordered_map<K,V>;

#endif /* SOURCE_GLOBAL_H_ */

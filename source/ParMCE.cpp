/*
 * ParMCEDegree.cpp
 *
 *  Created on: Oct 2, 2018
 *      Author: apurba
 */

#include "ParMCE.h"

ParMCE::ParMCE() {
	// TODO Auto-generated constructor stub
}

ParMCE::ParMCE(const undirectedgraph &g) {
	_g_ = g;
	cliquecount = 0;
}

void ParMCE::seq_expand(std::vector<int> k, std::unordered_set<int> &cand,
		std::unordered_set<int> &fini) {
	if (cand.empty() && fini.empty()) {

		cliquecount.fetch_and_increment();

		if (cliquecount % 100000000 == 0){
			ofstream outfile;
			outfile.open(of.c_str(), ios::app);
			outfile << cliquecount << " maximal cliques generated\n";
			outfile.close();
		}

		return;
	}
	if (cand.empty())
		return;

	//cout << __LINE__ << " Inside expand, before calling findPivot\n";

	int pivot = findPivot(cand, fini);

	//cout << __LINE__ << " Inside expand, after calling findPivot\n";

	auto cand_iterator = cand.begin();
	const auto cand_end_iterator = cand.end();
	const auto neighbor_of_pivot_end_iterator = _g_.neighbor(pivot).end();
	while (cand_iterator != cand_end_iterator) {

		if (_g_.neighbor(pivot).find(*cand_iterator)
				== neighbor_of_pivot_end_iterator) {

			k.push_back(*cand_iterator);

			unordered_set<int> cand_q;
			utils::unordered_intersect(cand, _g_.neighbor(*cand_iterator),
					&cand_q);
			unordered_set<int> fini_q;
			utils::unordered_intersect(fini, _g_.neighbor(*cand_iterator),
					&fini_q);

			seq_expand(k, cand_q, fini_q);

			fini.insert(*cand_iterator);
			cand_iterator = cand.erase(cand_iterator);
			k.pop_back();
		} else {
			cand_iterator++;
		}

	}
}

void ParMCE::expand(std::vector<int> k, std::unordered_set<int> &cand,
		std::unordered_set<int> &fini) {
	if (cand.empty() && fini.empty()) {

		cliquecount.fetch_and_increment();	//atomic increment

		if (cliquecount % 100000000 == 0){
			ofstream outfile;
			outfile.open(of.c_str(), ios::app);
			outfile << cliquecount << " maximal cliques generated\n";
			outfile.close();
		}

		return;
	}
	if (cand.empty() && !fini.empty())
		return;

	int pivot;

	//if (cand.size() + fini.size() > 400)
		pivot = ParFindPivot(cand, fini, _g_);
	//else
	//	pivot = findPivot(cand, fini);

	vector<int> ext;

	const auto neighbor_of_pivot_end_iterator = _g_.neighbor(pivot).end();

	std::unordered_map<int, int> exttoindex;

	int index = 0;
	for (int w : cand) {
		if (_g_.neighbor(pivot).find(w) == neighbor_of_pivot_end_iterator) {
			ext.push_back(w);
			exttoindex.insert( { w, index });
			index++;
		}
	}

	tbb::parallel_for(tbb::blocked_range<int>(0, ext.size()),
			[&](tbb::blocked_range<int> r) {
				for(int idx = r.begin(); idx < r.end(); ++idx) {
					int q = ext[idx];

					vector<int> k_q = k;

					k_q.push_back(q);

					unordered_set<int> cand_q;
					unordered_set<int> fini_q;

					computeNewCand(cand, _g_.neighbor(q), exttoindex, idx, &cand_q);
					computeNewFini(fini, _g_.neighbor(q), exttoindex, ext, idx, &fini_q);

					if(cand_q.size()+fini_q.size() > 100)
					expand(k_q, cand_q, fini_q);
					else
					seq_expand(k_q, cand_q, fini_q);

				}
			});

}

void ParMCE::computeNewCand(const unordered_set<int> &cand,
		const unordered_set<int> &nghofq, const unordered_map<int, int> &ext,
		int idx, unordered_set<int> *result) {

	if (cand.size() > nghofq.size()) {
		for (int w : nghofq) {
			if (cand.count(w) > 0) {
				int num_w_in_ext = ext.count(w);
				if ((num_w_in_ext > 0) && (ext.at(w) > idx))
					(*result).insert(w);
				if (num_w_in_ext == 0)
					(*result).insert(w);
			}
		}
	} else {
		for (int w : cand) {
			if (nghofq.count(w) > 0) {
				int num_w_in_ext = ext.count(w);
				if ((num_w_in_ext > 0) && (ext.at(w) > idx))
					(*result).insert(w);
				if (num_w_in_ext == 0)
					(*result).insert(w);
			}
		}
	}
}

void ParMCE::computeNewFini(const unordered_set<int> &fini,
		const unordered_set<int> &nghofq, const unordered_map<int, int> &ext,
		const vector<int>& extvec, int idx, unordered_set<int> *result) {

	if (nghofq.size() < fini.size() + idx) {
		for (int w : nghofq) {
			if (fini.count(w) > 0) {
				(*result).insert(w);
			}
			if ((ext.count(w) > 0) && (ext.at(w) < idx))
				(*result).insert(w);
		}
	} else {
		for (int w : fini) {
			if (nghofq.count(w) >  0)
				(*result).insert(w);
		}
		for (int i = 0; i < idx; i++) {
			int x = extvec[i];
			if (nghofq.count(x) > 0)
				(*result).insert(x);
		}
	}
}

int ParMCE::findPivot(const unordered_set<int> &cand,
		const unordered_set<int> &fini) {

	int size = -1;
	int p = -1;

	for (int u : cand) {
		if((int)_g_.neighbor(u).size() < size)
			continue;
		int size_of_q = utils::unordered_intersect_size(_g_.neighbor(u), cand);
		if (size < size_of_q) {
			size = size_of_q;
			p = u;
		}
	}

	for (int u : fini) {
		if((int)_g_.neighbor(u).size() < size)
			continue;
		int size_of_q = utils::unordered_intersect_size(_g_.neighbor(u), cand);
		if (size < size_of_q) {
			size = size_of_q;
			p = u;
		}
	}

	return p;
}

int ParMCE::ParFindPivot(std::unordered_set<int> &cand,
		std::unordered_set<int> &fini, undirectedgraph &g) {

	//cout << "Inside function " << __func__ << "\n";

	ConcurrentTable v_to_intersect_size;

	tbb::parallel_for_each(cand.begin(), cand.end(),
			[&](int u) {

				int intersect_size = utils::unordered_intersect_size(_g_.neighbor(u), cand);

				tbb::concurrent_hash_map<int, int>::accessor ac;

				v_to_intersect_size.insert(ac, u);//inserted as (value, key) pair in concurrent_hash_map

				ac->second = intersect_size;
			});

	tbb::parallel_for_each(fini.begin(), fini.end(),
			[&](int u) {

				int intersect_size = utils::unordered_intersect_size(_g_.neighbor(u), cand);

				tbb::concurrent_hash_map<int, int>::accessor ac;

				v_to_intersect_size.insert(ac, u);

				ac->second = intersect_size;
			});

	int size = -1;
	int v = 0;

	for (ConcurrentTable::iterator i = v_to_intersect_size.begin();
			i != v_to_intersect_size.end(); i++) {
		int u = i->first;
		int tmp_size = i->second;

		if (size < tmp_size) {
			size = tmp_size;
			v = u;
		}
	}

	return v;

}

void ParMCE::runOnDegreeOrdering(int nthreads, string outfile) {

	tbb::task_scheduler_init init(nthreads);

	of = outfile;

	tbb::parallel_for_each(_g_._AdjList_.begin(), _g_._AdjList_.end(), [&](pair<int, unordered_set<int> > p) {

		std::vector<int> k;
		std::unordered_set<int> cand;
		std::unordered_set<int> fini;

		int v = p.first;

		for(int w : _g_.neighbor(v)) {

			if (_g_.degreeOf(w) > _g_.degreeOf(v))
			cand.insert(w);
			else if (_g_.degreeOf(w) < _g_.degreeOf(v))
			fini.insert(w);
			else {
				if (w > v)
				cand.insert(w);
				else
				fini.insert(w);
			}
		}

		if(cand.size()+fini.size() > 100)
		expand(k, cand, fini);
		else
		seq_expand(k, cand, fini);

	});

}

void ParMCE::runSequential() {


	cout << "This is to measure the irregularities of the subproblems.\n";

	cout << "Running on each subproblem sequentially. Using ranking based on lexicographic ordering.\n";

	for(map<int, unordered_set<int> >::iterator iter = _g_._AdjList_.begin(); iter != _g_._AdjList_.end(); iter++){

		std::vector<int> k;
		std::unordered_set<int> cand;
		std::unordered_set<int> fini;

		int v = iter->first;

		for(int w : _g_.neighbor(v)) {

			if (w > v)
				cand.insert(w);
			else if (w < v)
				fini.insert(w);
		}

		cliquecount = 0;

		auto t_start = std::chrono::high_resolution_clock::now();

		seq_expand(k, cand, fini);

		auto t_end = std::chrono::high_resolution_clock::now();

		auto elapsed = std::chrono::duration<double, std::milli>(t_end - t_start).count();

		cout << v << "," << cliquecount << "," << elapsed << "\n";

	}

}

void ParMCE::runELS() {


	cout << "This algorithm is due to Eppestein, Loffler, Strass\n";
	
	_g_.degeneracy();

	//cout << "Running on each subproblem sequentially. Using ranking based on lexicographic ordering.\n";

	for(map<int, unordered_set<int> >::iterator iter = _g_._AdjList_.begin(); iter != _g_._AdjList_.end(); iter++){

		std::vector<int> k;
		std::unordered_set<int> cand;
		std::unordered_set<int> fini;

		int v = iter->first;

		for(int w : _g_.neighbor(v)) {
			

			if (_g_.degeneracyOf(w) > _g_.degeneracyOf(v))
				cand.insert(w);
			else if (_g_.degeneracyOf(w) < _g_.degeneracyOf(v))
				fini.insert(w);
			else {
				if (w > v)
				cand.insert(w);
				else
				fini.insert(w);
			}

		}

		//cliquecount = 0;

		//auto t_start = std::chrono::high_resolution_clock::now();

		seq_expand(k, cand, fini);

		//auto t_end = std::chrono::high_resolution_clock::now();

		//auto elapsed = std::chrono::duration<double, std::milli>(t_end - t_start).count();

		//cout << v << "," << cliquecount << "," << elapsed << "\n";

	}

}

void ParMCE::runOnDegeneracyOrdering(int nthreads) {
	
	tbb::task_scheduler_init init(nthreads);
	
	auto t_start = std::chrono::high_resolution_clock::now();
	
	_g_.degeneracy();
	
	auto t_end = std::chrono::high_resolution_clock::now();

	auto degeneracy_compute_time = std::chrono::duration<double, std::milli>(t_end - t_start).count();

	cout << "Degeneracy computation time: " << degeneracy_compute_time/1000 << " sec.\n";

	tbb::parallel_for_each(_g_._AdjList_.begin(), _g_._AdjList_.end(), [&](pair<int, unordered_set<int> > p) {

		std::vector<int> k;
		std::unordered_set<int> cand;
		std::unordered_set<int> fini;

		int v = p.first;

		for(int w : _g_.neighbor(v)) {

			if (_g_.degeneracyOf(w) > _g_.degeneracyOf(v))
			cand.insert(w);
			else if (_g_.degeneracyOf(w) < _g_.degeneracyOf(v))
			fini.insert(w);
			else {
				if (w > v)
				cand.insert(w);
				else
				fini.insert(w);
			}
		}

		if(cand.size()+fini.size() > 100)
		expand(k, cand, fini);
		else
		seq_expand(k, cand, fini);

	});

}

void ParMCE::runOnTriangleOrdering(int nthreads) {
	
	tbb::task_scheduler_init init(nthreads);

	//_g_.triangleCount();

	tbb::parallel_for_each(_g_._AdjList_.begin(), _g_._AdjList_.end(), [&](pair<int, unordered_set<int> > p) {

		std::vector<int> k;
		std::unordered_set<int> cand;
		std::unordered_set<int> fini;
		
		int v = p.first;

		for(int w : _g_.neighbor(v)) {

			if (_g_.triangleCountAt(w) > _g_.triangleCountAt(v))
			cand.insert(w);
			else if (_g_.triangleCountAt(w) < _g_.triangleCountAt(v))
			fini.insert(w);
			else {
				if (w > v)
				cand.insert(w);
				else
				fini.insert(w);
			}
		}

		if(cand.size()+fini.size() > 100)
		expand(k, cand, fini);
		else
		seq_expand(k, cand, fini);

	});

}

long ParMCE::numberOfCliques() {
	return cliquecount;
}

ParMCE::~ParMCE() {
	// TODO Auto-generated destructor stub
}


/*
 * PECO.h
 *
 *  Created on: Nov 1, 2018
 *      Author: apurba
 */


#ifndef SOURCE_PECO_H_
#define SOURCE_PECO_H_

#include <unordered_set>
#include <vector>

#include <tbb/atomic.h>
#include <tbb/parallel_for_each.h>

#include "utils.h"
#include "undirectedgraph.h"

using namespace std;

class PECO {
	undirectedgraph _g_;
	tbb::atomic<long> cliquecount;
	void expand(undirectedgraph &, std::vector<int>, std::unordered_set<int> &, std::unordered_set<int> &);
	int findPivot(undirectedgraph&, const unordered_set<int> &, const unordered_set<int> &);
public:
	PECO();
	PECO(const undirectedgraph &);
	void runOnDegreeOrdering();
	void runOnDegeneracyOrdering();
	void runOnTriangleOrdering();
	long numberOfCliques();
	virtual ~PECO();
};

#endif /* SOURCE_PECO_H_ */

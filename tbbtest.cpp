/*
 * tbbtest.cc
 *
 *  Created on: Sep 25, 2018
 *      Author: apurba
 */
#include<iostream>
#include <string>
#include <ctime>
#include <chrono>
#include "tbb/task_scheduler_init.h"

#include "source/sorttest.h"

#include "source/EmbarrasingParallel.h"

#include "source/ParTomita.h"

#include "source/undirectedgraph.h"

int main(int argc, char* argv[]){

	EmbarrasingParallel ep;

	//clock_t begin = clock();
	
	auto t_start = std::chrono::high_resolution_clock::now();

	ep.ParallelExp_1(std::stol(argv[1]));

	//clock_t end = clock();
	
	auto t_end = std::chrono::high_resolution_clock::now();

	//double duration = 1000.0 * (double)(end-begin) / CLOCKS_PER_SEC;
	
	auto pduration = std::chrono::duration<double, std::milli>(t_end - t_start).count();


	//begin = clock();
	t_start = std::chrono::high_resolution_clock::now();

	ep.Exp(std::stol(argv[1]));

	//end = clock();
	t_end = std::chrono::high_resolution_clock::now();

	//duration = 1000.0 * (double)(end-begin) / CLOCKS_PER_SEC;
	auto sduration = std::chrono::duration<double, std::milli>(t_end - t_start).count();

	std::cout << "Computing PI " << std::stol(argv[1]) << " times in parallel take " << pduration << " ms.\n";
	std::cout << "Computing PI " << std::stol(argv[1]) << " times take " << sduration << " ms.\n";







	return 0;
}





/*
 * mpi_hello_world_main.cpp
 *
 *  Created on: March 4, 2019
 *      Author: Apurba
 */

#include <iostream>
#include<unordered_set>
#include<vector>
#include <chrono>
#include <mpi.h>
#include <omp.h>
using namespace std;


int main(int argc, char **argv){

        int size, rank;

	long global_count, local_count;

	char processor_name[MPI_MAX_PROCESSOR_NAME];
    	int processor_name_len;


        MPI_Init(NULL, NULL);
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(processor_name, &processor_name_len);

	cout << "size: " << size << "\n\n";

	cout << "rank: " << rank << "\n\n";

	cout << "Saying Hello World from processor: " << processor_name << "\n\n";


        MPI_Finalize();

	return 0;

}

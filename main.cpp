/*
 * main.cpp
 *
 *  Created on: Sep 24, 2018
 *      Author: apurba
 */

#include <iostream>
#include<unordered_set>
#include<vector>
#include <chrono>
using namespace std;

#include "source/tomita.h"
#include "source/undirectedgraph.h"
#include "source/sorttest.h"
#include "source/ParTomita.h"
#include "source/ParMCE.h"
#include "source/PECO.h"

int main(int argc, char **argv){

	undirectedgraph g;

	cout << argv[1] << "\n";

	//g.readInAdjList(argv[1]);

	auto t_start = std::chrono::high_resolution_clock::now();

	g.readInEdgeList(argv[1]);

	auto t_end = std::chrono::high_resolution_clock::now();

	auto graph_reading_time = std::chrono::duration<double, std::milli>(t_end - t_start).count();


	//g.print();

	cout << "number of vertices: " << g.numV() << "\n";
	cout << "number of edges: " << g.numE() << "\n";


	cout << "calling tomita" << "\n";

	t_start = std::chrono::high_resolution_clock::now();

	tomita t(g);

	t_end = std::chrono::high_resolution_clock::now();

	auto tomita_initialization_time = std::chrono::duration<double, std::milli>(t_end - t_start).count();


	t_start = std::chrono::high_resolution_clock::now();

	t.run();

	t_end = std::chrono::high_resolution_clock::now();

	auto max_clique_time_tomita = std::chrono::duration<double, std::milli>(t_end - t_start).count();

	auto num_maxc_tomita = t.numberOfCliques();


	cout << "calling ParTomita\n";

	ParTomita pt(g);

	t_start = std::chrono::high_resolution_clock::now();

	pt.run();

	t_end = std::chrono::high_resolution_clock::now();


	auto max_clique_time_partomita = std::chrono::duration<double, std::milli>(t_end - t_start).count();

	auto num_maxc_partomita = pt.numberOfCliques();


	cout << "calling PECO\n";

	PECO peco(g);

	t_start = std::chrono::high_resolution_clock::now();

	peco.runOnDegreeOrdering();

	t_end = std::chrono::high_resolution_clock::now();

	auto max_clique_time_peco = std::chrono::duration<double, std::milli>(t_end - t_start).count();

	auto num_maxc_peco = peco.numberOfCliques();


	cout << "calling ParMCE\n\n\n\n";

	ParMCE parmce(g);

	t_start = std::chrono::high_resolution_clock::now();

	parmce.runOnDegreeOrdering();

	t_end = std::chrono::high_resolution_clock::now();

	auto max_clique_time_parmce = std::chrono::duration<double, std::milli>(t_end - t_start).count();

	auto num_maxc_parmce = parmce.numberOfCliques();


	cout << "\t\tResults of Different Algorithms for MCE on static graph\n\n";

	cout << "Algorithm\tNum. Maximal Cliques\tEnum Time (sec.)\tParallelSpeedup\n";

	cout << "Tomita\t" << num_maxc_tomita << "\t" << max_clique_time_tomita/1000 << "\t" << "NA\n";
	cout << "ParTomita\t" << num_maxc_partomita << "\t" << max_clique_time_partomita/1000 << "\t" << max_clique_time_tomita/max_clique_time_partomita <<"\n";
	cout << "PECO\t" << num_maxc_peco << "\t" << max_clique_time_peco/1000 << "\t" << max_clique_time_tomita/max_clique_time_peco <<"\n";
	cout << "ParMCE\t" << num_maxc_parmce << "\t" << max_clique_time_parmce/1000 << "\t" << max_clique_time_tomita/max_clique_time_parmce <<"\n";





}

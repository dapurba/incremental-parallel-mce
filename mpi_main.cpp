/*
 * mpi_main.cpp
 *
 *  Created on: Sep 24, 2018
 *      Author: apurba
 */

#include <iostream>
#include<unordered_set>
#include<vector>
#include <chrono>
#include <mpi.h>
#include <omp.h>
using namespace std;

#include "source/tomita.h"
#include "source/undirectedgraph.h"
#include "source/sorttest.h"
#include "source/ParTomita.h"
#include "source/ParMCE.h"
#include "source/PECO.h"
#include "source/HybMCE.h"

int main(int argc, char **argv){

	undirectedgraph g;
        int size, rank;

	long global_count, local_count;

	char processor_name[MPI_MAX_PROCESSOR_NAME];
        int processor_name_len;


        MPI_Init(NULL, NULL);
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(processor_name, &processor_name_len);


        
        

	 //g.readInAdjList(argv[1]);

	auto t_start = std::chrono::high_resolution_clock::now();

	g.readInEdgeList(argv[1]);

	auto t_end = std::chrono::high_resolution_clock::now();

	auto graph_reading_time = std::chrono::duration<double, std::milli>(t_end - t_start).count();
	//double startOMP = omp_get_wtime();
        double start = MPI_Wtime();

        HybMCE hybmce(g, size);
        hybmce.runOnDegreeOrdering(rank, stoi(argv[2]));


        auto hybmce_enumeration_time = MPI_Wtime()-start;
        auto total = hybmce_enumeration_time;

        //double finishOMP = omp_get_wtime() - startOMP;

        auto hybmce_clique_count = hybmce.numberOfCliques();

        local_count = hybmce_clique_count;

	cout << "rank: " << rank << "\n\n";
        cout << "processor name: " << processor_name << "\n\n";
        cout << argv[1] << "\n";
        cout << "number of vertices: " << g.numV() << "\n";
        cout << "number of edges: " << g.numE() << "\n\n";
        cout << "Algorithm HybMCE\n";
        cout << "Graph reading time: " << graph_reading_time << "ms.\n";
        cout << "Number of maximal cliques: " << hybmce_clique_count << "\n";
        cout << "Clique enumeration time (MPI): " << hybmce_enumeration_time << "sec.\n";
        //cout << "Clique enumeration time (OMP): " << finishOMP << "sec.\n\n";


        double timeMin, timeMax;

        MPI_Reduce(&total, &timeMax, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
        MPI_Reduce(&total, &timeMin, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);

	MPI_Reduce(&local_count, &global_count, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);

	if(rank == 0){
		cout << argv[1] << "\n";
		cout << "number of vertices: " << g.numV() << "\n";
		cout << "number of edges: " << g.numE() << "\n";
		cout << "Graph reading time: " << graph_reading_time << "ms.\n";
		cout << "Total number of maximal cliques: " << global_count << "\n";
                cout << "Max. time: " << timeMax << "\n";
                cout << "Min. time: " << timeMin << "\n\n";

	}


        MPI_Finalize();

}
